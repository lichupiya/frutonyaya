package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;

public class AuthorizationPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"USER_LOGIN\"]")
    WebElement userLoginInput;

    @FindBy(xpath = "//*[@name=\"USER_PASSWORD\"]")
    WebElement userPasswordInput;

    @FindBy(css = "[class*=\"formPage__submit_recover\"]")
    WebElement loginButton;

    @FindBy(xpath = "//*[text()='Вы авторизованы!']")
    WebElement authorizationPopup;

    @FindBy(xpath = "//*[text()='Неверный логин или пароль']")
    WebElement authorizationNegativePopup;


    public AuthorizationPage() {
        driver.get(config.baseUrl() + "/auth/");
        PageFactory.initElements(driver, this);
    }

    public AuthorizationPage inputLogin(String loginText) {
        userLoginInput.sendKeys(loginText);
        return this;
    }

    public AuthorizationPage inputPassword(String passwordText) {
        userPasswordInput.sendKeys(passwordText);
        return this;
    }

    public AuthorizationPage clickLoginButton() {
        loginButton.click();
        return this;
    }

    public AuthorizationPage assertAuthorizationPopup(String expectedAuthorizationText) throws InterruptedException {
        sleep(1000);
        assertEquals("Авторизация неуспешна", expectedAuthorizationText, authorizationPopup.getText());
        return this;
    }

    public AuthorizationPage assertAuthorizationNegativePopup(String expectedAuthorizationNegativeText) throws InterruptedException {
        sleep(1000);
        assertEquals("Авторизация неуспешна", expectedAuthorizationNegativeText, authorizationNegativePopup.getText());
        return this;
    }
}