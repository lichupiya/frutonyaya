import org.junit.Test;
import pages.AuthorizationPage;

public class AuthorizationTest extends BaseTest {

    @Test
    public void testAuthorizationPositive() throws InterruptedException{
        String userLogin = "cpk54348@fosiq.com";
        String userPassword = "qwerty$444";
        String expectedAuthorizationText = "Вы авторизованы!";

        AuthorizationPage authorizationPage = new AuthorizationPage();

        authorizationPage
                .inputLogin(userLogin)
                .inputPassword(userPassword)
                .clickLoginButton()
                .assertAuthorizationPopup(expectedAuthorizationText);
    }

    @Test
    public void testAuthorizationIncorrectPassword() throws InterruptedException{
        String userIncorrectLogin = "test@test.test";
        String userIncorrectPassword = "test";
        String expectedAuthorizationNegativeText = "Неверный логин или пароль";

        AuthorizationPage authorizationPage = new AuthorizationPage();

        authorizationPage
                .inputLogin(userIncorrectLogin)
                .inputPassword(userIncorrectPassword)
                .clickLoginButton()
                .assertAuthorizationNegativePopup(expectedAuthorizationNegativeText);
    }
}